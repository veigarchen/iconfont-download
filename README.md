# iconfont-一键下载

> iconfont-一键下载插件，提供vue3组件式按需编译使用（单色、多色配置），右上角点点小心心，一年赚他一个小目标~ 
> 
> 推荐项目：[vue3按需使用iconfont示例](https://gitee.com/veigarchen/iconfont-download-vue3)、[uniapp按需使用iconfont示例和快速开发小程序](https://gitee.com/veigarchen/uniapp-vue3-fast)
> 
> 前端技术交流群：<a href="https://jq.qq.com/?_wv=1027&k=Erh7LBKn" target="_blank">553655769</a>，白嫖更多资源，行业交流
> 
>  注意：本插件仅作为下载工具，如果图标未注明可免费商用，请自行联系相关图标作者，否则后果自负

## 安装
```bash
以谷歌浏览器-版本-108.0.5359.71为例
1、打开浏览器的扩展程序页面（浏览器右上角三个点->更多工具->扩展程序）
2、打开开发者模式，点击加载已解压的扩展程序，选择本项目目录
```

## 使用

```bash
在浏览器右上角点击插件图标，点击iconfont即可使用
```

## 功能列表

1、下载本页所有svg
<img src="使用教程/5.png" />

2、导出为vue3组件使用及icon的ts文件

3、配置打包时的压缩数率，范围1-6，1时体积最小，6最大
<img src="使用教程/压缩效果图.png" />

4、vue3按需编译使用iconfont图标：[按需编译插件示例](https://gitee.com/veigarchen/iconfont-download-vue3)

<img src="使用教程/build-1.png" />

## 关于&合作

```bash
QQ/微信：1518079148
```